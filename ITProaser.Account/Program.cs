using System.Globalization;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.DataProtection;
using Microsoft.EntityFrameworkCore;
using Microsoft.Net.Http.Headers;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
string connectionString = builder.Configuration.GetConnectionString("DefaultConnection") ??
                       throw new InvalidOperationException("Connection string 'DefaultConnection' not found.");
string? provider = builder.Configuration.GetValue<string>("Provider");

var localizationOptions = new Action<RequestLocalizationOptions>(options =>
{
    var supportedCultures = new List<CultureInfo> { new("en"), new("es") };
    options.SetDefaultCulture("en");
    options.ApplyCurrentCultureToResponseHeaders = true;
    options.SupportedCultures = options.SupportedUICultures = supportedCultures;
});

// Localization service
builder.Services.AddRequestLocalization(localizationOptions).AddLocalization(options => options.ResourcesPath = "Resources");

builder.Services.AddDbContext<AccountDbContext>(options => _ = provider switch
    {
        "SqlServer" => options.UseSqlServer(connectionString, sql => sql.MigrationsAssembly("ITProaser.Models.Database.SqlServer")),
        "Postgres" => options.UseNpgsql(connectionString, pg => pg.MigrationsAssembly("ITProaser.Models.Database.Postgres")),
        _ => throw new NotSupportedException($"Database engine {provider} is not supported.")
    }
);
builder.Services.AddDataProtection().SetApplicationName("ITProaser");

builder.Services.AddCors(cors => cors.AddDefaultPolicy(policy
    => policy.WithMethods(HttpMethods.Get, HttpMethods.Post, HttpMethods.Put, HttpMethods.Delete)
        .WithHeaders(HeaderNames.AcceptLanguage).AllowCredentials()));

builder.Services.AddDefaultIdentity<User>(options => options.SignIn.RequireConfirmedAccount = options.User.RequireUniqueEmail = true)
    .AddEntityFrameworkStores<AccountDbContext>();

builder.Services.AddIdentityServer()
    .AddApiAuthorization<User, AccountDbContext>();

builder.Services.AddAuthentication()
    .AddIdentityServerJwt();

builder.Services.AddControllers();

builder.Services.AddResponseCompression().AddResponseCaching();

builder.Services.AddSpaStaticFiles(options => options.RootPath = "ClientApp/dist");

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
    app.UseMigrationsEndPoint();
else
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();

app.UseRequestLocalization(localizationOptions);

app.UseHttpsRedirection();
app.UseStaticFiles();
app.UseSpaStaticFiles();
app.UseResponseCaching();
app.UseResponseCompression();
app.UseRouting();

app.UseAuthentication();
app.UseIdentityServer();
app.UseAuthorization();
app.UseCors();
app.MapControllerRoute(
    name: "default",
    pattern: "{controller}/{action=Index}/{id?}");

app.UseSpa(spa =>
{
    (spa.Options.DevServerPort, spa.Options.SourcePath) = (3139, "ClientApp");
    if (app.Environment.IsDevelopment())
        spa.UseProxyToSpaDevelopmentServer("http://localhost:3139");
});


app.Run();