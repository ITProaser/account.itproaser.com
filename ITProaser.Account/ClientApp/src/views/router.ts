import {createRouter, createWebHistory, RouteRecordRaw} from "vue-router";

const IndexView = () => import("./IndexView.vue");
const AboutView = () => import("./AboutView.vue");
const LogInView = () => import("./LogInView.vue");
const SignUpView = () => import("./SignUpView.vue");

const routes: RouteRecordRaw[] = [
    {
        path: "/",
        component: IndexView
    },
    {
        path: "/about",
        component: AboutView
    },
    {
        path: "/login",
        component: LogInView
    },
    {
        path: "/signup",
        component: SignUpView
    }
];

const router = createRouter({
    history: createWebHistory(),
    routes
});

export default router;
