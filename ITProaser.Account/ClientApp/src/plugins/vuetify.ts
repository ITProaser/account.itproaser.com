/**
 * plugins/vuetify.ts
 *
 * Framework documentation: https://vuetifyjs.com`
 */

// Styles
import "@mdi/font/css/materialdesignicons.css";
import "vuetify/styles";

// Composables
import { createVuetify } from "vuetify";
import {md3} from "vuetify/blueprints";

const palette = {
    primary: "#9E77ED", "primary-dark": "#53389E", "primary-light": "#D6BBFB",
    secondary: "#528BFF", "secondary-dark": "#155EEF", "secondary-light": "#B2CCFF",
    tertiary: "#CDD5DF", "tertiary-dark": "#697586", "tertiary-light": "#E3E8EF"
};

const light = {
    dark: false,
    background: "#FFFFFF",
    surface: "#F8F9FA",
    ...palette
};

const dark = {
    dark: true,
    background: "#202124",
    surface: "#303030",
    ...palette
};

// https://vuetifyjs.com/en/introduction/why-vuetify/#feature-guides
export default createVuetify({
    blueprint: md3,
    theme: {
        defaultTheme: (window.matchMedia("(prefers-color-scheme: dark)").matches
            ? "dark"
            : "light")
        ,
        themes: { light, dark }
    }
});
