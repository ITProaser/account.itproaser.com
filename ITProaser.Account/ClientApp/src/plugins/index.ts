/**
 * plugins/index.ts
 *
 * Automatically included in `./src/main.ts`
 */

// Plugins
import { loadFonts } from "./webfontloader";
import router from "@/views/router";
import vuetify from "./vuetify";

// Types
import type { App } from "vue";

export async function registerPlugins (app: App) {
    app.use(router);
    await loadFonts();
    app.use(vuetify);
}
